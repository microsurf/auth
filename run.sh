#!/usr/bin/env bash

IMAGE_NAME=auth

cd ./src

if [[ "$(docker images -q $IMAGE_NAME 2> /dev/null)" == "" ]]; then
  docker rm $IMAGE_NAME
  docker build -t $IMAGE_NAME .
fi

docker run -p 5001:5001 $IMAGE_NAME