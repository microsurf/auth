FROM python:alpine
ENV PYTHONUNBUFFERED 1
ENV C_FORCE_ROOT true

RUN apk update && apk add build-base  ## git ?

RUN mkdir -p /local && mkdir -p /local/surf && mkdir -p /local/surf/auth
ADD ./src /local/surf/auth
WORKDIR /local/surf/auth

RUN pip install -r requirements.pip

CMD python3 -m sanic app.app --host=0.0.0.0 --port=5001 --workers=1