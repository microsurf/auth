job "auth" {

  datacenters = ["dc1"]
  type = "service"

  update {
    max_parallel = 1
    min_healthy_time = "10s"
    healthy_deadline = "3m"
    auto_revert = false
    canary = 0
  }

  migrate {
    max_parallel = 1
    health_check = "checks"
    min_healthy_time = "10s"
    healthy_deadline = "5m"
  }

  group "alpha" {

    count = 1

    restart {
      attempts = 1
      interval = "30m"
      delay = "15s"
      mode = "fail"
    }

    ephemeral_disk {
      sticky = true
      migrate = true
      size = 150
    }

    task "web" {

      driver = "raw_exec"

      env {
        PWD = "$(pwd)"
      }

      config {
        command = "/bin/sh"
        args = [
          "$(pwd)/${PWD}/run.sh"
        ]
      }

      resources {
        cpu = 2000
        memory = 768
        network {
          mbits = 100
          port "auth_port" {
            static = 5001
          }
        }
      }

      service {
        name = "auth"
        tags = ["global", "security", "auth", "db", "mongodb"]
        port = "auth_port"
        check {
          name = "alive"
          type = "tcp"
          interval = "10s"
          timeout = "2s"
        }
      }
    }
  }
}